import {DefaultCrudRepository} from '@loopback/repository';
import {Localidad, LocalidadRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LocalidadRepository extends DefaultCrudRepository<
  Localidad,
  typeof Localidad.prototype.id,
  LocalidadRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Localidad, dataSource);
  }
}
