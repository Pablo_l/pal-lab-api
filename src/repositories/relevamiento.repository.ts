import {DefaultCrudRepository} from '@loopback/repository';
import {Relevamiento, RelevamientoRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RelevamientoRepository extends DefaultCrudRepository<
  Relevamiento,
  typeof Relevamiento.prototype.id,
  RelevamientoRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Relevamiento, dataSource);
  }
}
