import {DefaultCrudRepository} from '@loopback/repository';
import {Caracter, CaracterRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class CaracterRepository extends DefaultCrudRepository<
  Caracter,
  typeof Caracter.prototype.id,
  CaracterRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Caracter, dataSource);
  }
}
