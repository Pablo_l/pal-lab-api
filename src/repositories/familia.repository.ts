import {DefaultCrudRepository} from '@loopback/repository';
import {Familia, FamiliaRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FamiliaRepository extends DefaultCrudRepository<
  Familia,
  typeof Familia.prototype.id,
  FamiliaRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Familia, dataSource);
  }
}
