import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Familia} from '../models';
import {FamiliaRepository} from '../repositories';

export class FamiliaController {
  constructor(
    @repository(FamiliaRepository)
    public familiaRepository : FamiliaRepository,
  ) {}

  @post('/familias', {
    responses: {
      '200': {
        description: 'Familia model instance',
        content: {'application/json': {schema: getModelSchemaRef(Familia)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Familia, {
            title: 'NewFamilia',
            exclude: ['id'],
          }),
        },
      },
    })
    familia: Omit<Familia, 'id'>,
  ): Promise<Familia> {
    return this.familiaRepository.create(familia);
  }

  @get('/familias/count', {
    responses: {
      '200': {
        description: 'Familia model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Familia) where?: Where<Familia>,
  ): Promise<Count> {
    return this.familiaRepository.count(where);
  }

  @get('/familias', {
    responses: {
      '200': {
        description: 'Array of Familia model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Familia, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Familia) filter?: Filter<Familia>,
  ): Promise<Familia[]> {
    return this.familiaRepository.find(filter);
  }

  @patch('/familias', {
    responses: {
      '200': {
        description: 'Familia PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Familia, {partial: true}),
        },
      },
    })
    familia: Familia,
    @param.where(Familia) where?: Where<Familia>,
  ): Promise<Count> {
    return this.familiaRepository.updateAll(familia, where);
  }

  @get('/familias/{id}', {
    responses: {
      '200': {
        description: 'Familia model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Familia, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Familia, {exclude: 'where'}) filter?: FilterExcludingWhere<Familia>
  ): Promise<Familia> {
    return this.familiaRepository.findById(id, filter);
  }

  @patch('/familias/{id}', {
    responses: {
      '204': {
        description: 'Familia PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Familia, {partial: true}),
        },
      },
    })
    familia: Familia,
  ): Promise<void> {
    await this.familiaRepository.updateById(id, familia);
  }

  @put('/familias/{id}', {
    responses: {
      '204': {
        description: 'Familia PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() familia: Familia,
  ): Promise<void> {
    await this.familiaRepository.replaceById(id, familia);
  }

  @del('/familias/{id}', {
    responses: {
      '204': {
        description: 'Familia DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.familiaRepository.deleteById(id);
  }
}
