import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Caracter} from '../models';
import {CaracterRepository} from '../repositories';

export class CaracterController {
  constructor(
    @repository(CaracterRepository)
    public caracterRepository : CaracterRepository,
  ) {}

  @post('/caracters', {
    responses: {
      '200': {
        description: 'Caracter model instance',
        content: {'application/json': {schema: getModelSchemaRef(Caracter)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Caracter, {
            title: 'NewCaracter',
            exclude: ['id'],
          }),
        },
      },
    })
    caracter: Omit<Caracter, 'id'>,
  ): Promise<Caracter> {
    return this.caracterRepository.create(caracter);
  }

  @get('/caracters/count', {
    responses: {
      '200': {
        description: 'Caracter model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Caracter) where?: Where<Caracter>,
  ): Promise<Count> {
    return this.caracterRepository.count(where);
  }

  @get('/caracters', {
    responses: {
      '200': {
        description: 'Array of Caracter model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Caracter, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Caracter) filter?: Filter<Caracter>,
  ): Promise<Caracter[]> {
    return this.caracterRepository.find(filter);
  }

  @patch('/caracters', {
    responses: {
      '200': {
        description: 'Caracter PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Caracter, {partial: true}),
        },
      },
    })
    caracter: Caracter,
    @param.where(Caracter) where?: Where<Caracter>,
  ): Promise<Count> {
    return this.caracterRepository.updateAll(caracter, where);
  }

  @get('/caracters/{id}', {
    responses: {
      '200': {
        description: 'Caracter model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Caracter, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Caracter, {exclude: 'where'}) filter?: FilterExcludingWhere<Caracter>
  ): Promise<Caracter> {
    return this.caracterRepository.findById(id, filter);
  }

  @patch('/caracters/{id}', {
    responses: {
      '204': {
        description: 'Caracter PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Caracter, {partial: true}),
        },
      },
    })
    caracter: Caracter,
  ): Promise<void> {
    await this.caracterRepository.updateById(id, caracter);
  }

  @put('/caracters/{id}', {
    responses: {
      '204': {
        description: 'Caracter PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() caracter: Caracter,
  ): Promise<void> {
    await this.caracterRepository.replaceById(id, caracter);
  }

  @del('/caracters/{id}', {
    responses: {
      '204': {
        description: 'Caracter DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.caracterRepository.deleteById(id);
  }
}
