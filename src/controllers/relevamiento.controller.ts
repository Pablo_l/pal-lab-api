import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Relevamiento} from '../models';
import {RelevamientoRepository} from '../repositories';

export class RelevamientoController {
  constructor(
    @repository(RelevamientoRepository)
    public relevamientoRepository : RelevamientoRepository,
  ) {}

  @post('/relevamientos', {
    responses: {
      '200': {
        description: 'Relevamiento model instance',
        content: {'application/json': {schema: getModelSchemaRef(Relevamiento)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Relevamiento, {
            title: 'NewRelevamiento',
            exclude: ['id'],
          }),
        },
      },
    })
    relevamiento: Omit<Relevamiento, 'id'>,
  ): Promise<Relevamiento> {
    return this.relevamientoRepository.create(relevamiento);
  }

  @get('/relevamientos/count', {
    responses: {
      '200': {
        description: 'Relevamiento model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Relevamiento) where?: Where<Relevamiento>,
  ): Promise<Count> {
    return this.relevamientoRepository.count(where);
  }

  @get('/relevamientos', {
    responses: {
      '200': {
        description: 'Array of Relevamiento model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Relevamiento, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Relevamiento) filter?: Filter<Relevamiento>,
  ): Promise<Relevamiento[]> {
    return this.relevamientoRepository.find(filter);
  }

  @patch('/relevamientos', {
    responses: {
      '200': {
        description: 'Relevamiento PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Relevamiento, {partial: true}),
        },
      },
    })
    relevamiento: Relevamiento,
    @param.where(Relevamiento) where?: Where<Relevamiento>,
  ): Promise<Count> {
    return this.relevamientoRepository.updateAll(relevamiento, where);
  }

  @get('/relevamientos/{id}', {
    responses: {
      '200': {
        description: 'Relevamiento model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Relevamiento, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Relevamiento, {exclude: 'where'}) filter?: FilterExcludingWhere<Relevamiento>
  ): Promise<Relevamiento> {
    return this.relevamientoRepository.findById(id, filter);
  }

  @patch('/relevamientos/{id}', {
    responses: {
      '204': {
        description: 'Relevamiento PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Relevamiento, {partial: true}),
        },
      },
    })
    relevamiento: Relevamiento,
  ): Promise<void> {
    await this.relevamientoRepository.updateById(id, relevamiento);
  }

  @put('/relevamientos/{id}', {
    responses: {
      '204': {
        description: 'Relevamiento PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() relevamiento: Relevamiento,
  ): Promise<void> {
    await this.relevamientoRepository.replaceById(id, relevamiento);
  }

  @del('/relevamientos/{id}', {
    responses: {
      '204': {
        description: 'Relevamiento DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.relevamientoRepository.deleteById(id);
  }
}
