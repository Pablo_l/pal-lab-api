import {Entity, model, property} from '@loopback/repository';

@model()
export class Localidad extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  cliudad: string;

  @property({
    type: 'string',
    required: true,
  })
  calle: string;

  @property({
    type: 'number',
    default: 0,
  })
  altura?: number;

  @property({
    type: 'string',
    default: '',
  })
  observacion?: string;

  constructor(data?: Partial<Localidad>) {
    super(data);
  }
}

export interface LocalidadRelations {
  // describe navigational properties here
}

export type LocalidadWithRelations = Localidad & LocalidadRelations;
