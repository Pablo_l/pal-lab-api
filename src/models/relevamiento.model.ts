import {Entity, model, property} from '@loopback/repository';

@model()
export class Relevamiento extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
    defaultFn: 'uuidv4',
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
  })
  codPalCtes?: string;

  @property({
    type: 'string',
    required: true,
  })
  especie: string;

  @property({
    type: 'string',
    required: true,
  })
  familia: string;

  @property({
    type: 'string',
    required: true,
  })
  coleccionista: string;

  @property({
    type: 'number',
    required: true,
  })
  localidad: number;

  @property({
    type: 'string',
    required: true,
  })
  caracter: string;

  @property({
    type: 'array',
    itemType: 'string',
    required: true,
  })
  imagenes: string[];

  @property({
    type: 'number',
    default: 0,
  })
  cantidadColectada?: number;

  @property({
    type: 'string',
    default: '',
  })
  hervario?: string;

  @property({
    type: 'string',
    default: '',
  })
  descripcion?: string;

  constructor(data?: Partial<Relevamiento>) {
    super(data);
  }
}

export interface RelevamientoRelations {
  // describe navigational properties here
}

export type RelevamientoWithRelations = Relevamiento & RelevamientoRelations;
