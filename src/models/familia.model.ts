import {Entity, model, property} from '@loopback/repository';

@model()
export class Familia extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  nombre: string;

  constructor(data?: Partial<Familia>) {
    super(data);
  }
}

export interface FamiliaRelations {
  // describe navigational properties here
}

export type FamiliaWithRelations = Familia & FamiliaRelations;
