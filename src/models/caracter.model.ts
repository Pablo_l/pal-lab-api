import {Entity, model, property} from '@loopback/repository';

@model()
export class Caracter extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  unidadPolinica: number;

  @property({
    type: 'number',
    required: true,
  })
  tipoApertura: number;

  @property({
    type: 'number',
    required: true,
  })
  cantidadDeAperturas: number;

  @property({
    type: 'number',
    required: true,
  })
  posicionDeAperturas: number;

  @property({
    type: 'number',
    required: true,
  })
  ejePolar: number;

  @property({
    type: 'number',
    required: true,
  })
  diametroEcuatorial: number;

  @property({
    type: 'number',
    required: true,
  })
  pe: number;

  @property({
    type: 'number',
    required: true,
  })
  forma: number;

  @property({
    type: 'number',
    required: true,
  })
  ambito: number;

  @property({
    type: 'number',
    required: true,
  })
  rangoDeTamanio: number;

  @property({
    type: 'number',
    required: true,
  })
  tamanio: number;

  @property({
    type: 'number',
    required: true,
  })
  exina: number;

  @property({
    type: 'number',
    required: true,
  })
  estructura: number;

  @property({
    type: 'number',
    required: true,
  })
  escultura: number;

  @property({
    type: 'string',
    default: '',
  })
  observaciones?: string;

  constructor(data?: Partial<Caracter>) {
    super(data);
  }
}

export interface CaracterRelations {
  // describe navigational properties here
}

export type CaracterWithRelations = Caracter & CaracterRelations;
