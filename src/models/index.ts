// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

export * from './todo.model';
export * from './relevamiento.model';
export * from './localidad.model';
export * from './familia.model';
export * from './caracter.model';
